#ifndef IAL_H
#define IAL_H

#include <string.h>
#include <stdint.h>

#include "symbol.h"
#include "string.h"

/* Knuth-Morris-Pratt substring search algorithm */

/*
 * Generate prefix table required for KMP algorithm.
 *
 * @param pattern Pattern used for generating the prefix table.
 * @param length Length of the matching part of the patter. ('\0' excluded)
 * @param pTable Allocated structure, with <length> size. Will contain the output.
 */
void kmp_gen_prefix(const char* pattern, int64_t length, int* pTable);

/*
 * Uses the Knuth-Morris-Pratt algorithm to search for the pattern in the sentence.
 *
 * @param sentence The heap being searched for the pattern.
 * @param pattern The pattern to be found in the sentence.
 * @return Returns the index of first found occurrence of pattern in the sentence (starting at 0!).
 *           If no matches were found, returns -1 instead.
 */
int64_t kmp_search(const string heap, const string patt);

/* Shell sort */

/*
 * Shell sort implementation, used for sorting character arrays.
 *
 * @param array Pointer to the character array.
 * @param size Size of given array.
 */
void shell_sort(char* array, int64_t size);

/*
 * Shell sort for sorting strings.
 *
 * @param str String to sort.
 */
static inline void str_shell_sort(string str)
{
    shell_sort(str->data, str->size);
}

/* Hash table */

/*
 * Symbol table implementation:
 */
typedef struct TSymbolTable
{
    // Array indexed by hash function. Each index contains pointer to first element of a symbol list.
    symbol_el_ptr * table;

    uint32_t filled;
    uint32_t capacity;
} sym_tbl;

/*
 * Hash function using the djb2 algorithm made by Dan Bernstein :
 *   http://www.cse.yorku.ca/~oz/hash.html
 *
 * @param str String input, '\0' terminated.
 * @return Returns hash for given string.
 */
static inline unsigned long _djb2(const unsigned char* str)
{
    unsigned long hash = 5381;
    int c;

    // Optimization, takes only first 15 characters
    int i = 0;
    while((c = *(str++)))
    {
        i++;
        hash = ((hash << 5) + hash) + c;
        if(i >= 15)
            break;
    }

    return hash;
}

/*
 * Hash function using the sdbm algorithm :
 *   http://www.cse.yorku.ca/~oz/hash.html
 *
 * @param str String input, '\0' terminated.
 * @return Returns hash for given string.
 */
static inline unsigned long _sdbm(const unsigned char* str)
{
    unsigned long hash = 0;
    int c;

    while ((c = *(str++)))
        hash = c + (hash << 6) + (hash << 16) - hash;

    return hash;
}

/*
 * Initialize symbol table. Starting size is ST_START_SIZE.
 *
 * @return Returns initialized symbol table, or NULL, if initialization failed.
 */
sym_tbl* st_init();

/*
 * Prints debug information about the symbol table.
 *
 * @param st Input symbol table
 */
void st_info(sym_tbl* st);

/*
 * Destroys the given symbol table. Changes the given pointer to NULL.
 *
 * @param st Symbol table to be destroyed.
 */
void st_free(sym_tbl** st);

/*
 * Run given function for each symbol in given symbol table.
 *
 * @param st Symbol table to operate on.
 * @param fun Function to execute.
 */
void st_foreach(sym_tbl* st, void(*fun)(symbol_ptr));

/*
 * Get index in hash table, using hash function.
 *
 * @param st Initialized symbol table.
 * @param symbol Indexed symbol.
 * @return Returns index in the given symbol table.
 */
int64_t st_get_index(sym_tbl* st, string key);

/*
 * Look for the symbol with given key.
 *
 * @param st Initialized symbol table.
 * @param key Identification of searched object.
 * @return Returns first found item, with the same key. Returns NULL, if no item was found.
 */
symbol_ptr st_lookup(sym_tbl* st, string key);

/*
 * Inserts given symbol into the symbol table. Returns NULL, if the symbol is already added.
 *
 * All pointers to symbols in this table
 *
 * @param st Initialized symbol table.
 * @param symbol Symbol being inserted.
 * @return Returns pointer to the symbol inside the symbol table, if successful. Else returns NULL.
 */
symbol_el_ptr st_insert(sym_tbl* st, symbol_ptr data, string key);

/* End - Hash table */

#endif
