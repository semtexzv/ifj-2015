RM=rm -f
CC=gcc
#CFLAGS= -g -D DEBUG -std=c11 -pedantic

CFLAGS= -std=c11 -O3 -D USE_DJB2
BIN=ifj
SRCS=$(wildcard *.c)
HDRS=$(wildcard *.h)


all: $(BIN)

$(BIN): $(SRCS) $(HDRS)
	$(CC) $(CFLAGS) $(SRCS) -o $(BIN)

clean: 
	$(RM) $(BIN)

test: $(TESTFILES) $(BIN)
	./$(BIN) test/fact_it.ifj
