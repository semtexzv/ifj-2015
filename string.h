#ifndef STRING_H
#define STRING_H

#include <string.h>
#include "main.h"

#define VEC_CUSTOM_NAME tstr
#define VEC_DATA_TYPE char
#define VEC_DEFAULT_VAL (0)
#include "vec_base.ht"
#undef VEC_DEFAULT_VAL
#undef VEC_DATA_TYPE
#undef VEC_CUSTOM_NAME

typedef tstr* string;

#define VEC_CUSTOM_NAME sList
#define VEC_DATA_TYPE string
#define VEC_DEFAULT_VAL (NULL)
#include "vec_base.ht"
#undef VEC_DEFAULT_VAL
#undef VEC_DATA_TYPE
#undef VEC_CUSTOM_NAME

/* Space for declarations specific for tstr */

/*
 * Initialize string from C-style string. Includes the terminal '\0'!!
 *
 * @param str C-style, '\0' terminated character vector.
 * @return Initialized string, needs to be freed;
 */
string c_str_to_str(const char* str);

/*
 * Initialize string from given string
 *
 * @param str Input string
 * @return Returns new string, needs to be freed!
 */
string str_to_str(const string str);

/*
 * Take string (character vector) and return c-type string
 *
 * @param str Input string
 * @return C-type string
 */
const char* str_to_c_str(string str);


/*
 * Take string (character vector) and COPY c-type string
 *
 * @param str Input string
 * @return C-type string
 */
char* str_copy_c_str(string str);

/*
 * Compare two strings (character vectors). Returns zero, if the strings match.
 *
 * Uses strncmp function from the standard library.
 *
 * @param str1 First string.
 * @param str2 Second string.
 * @return Returns value less than zero, if the first non-matching character had lower code in str1,
 *                       more than zero, if the first non-matching character had higher code in str1,
 *                       zero, if the strings matched.
 */
int32_t str_cmp(string str1, string str2);

/*
 * Return new string, containing str1, concatenated with str2.
 * Resulting string will ignore the first strings ending '\0' and add one to the end of
 *   second string, if needed.
 *
 * @param str1 First string
 * @param str2 Second string
 * @return Returns new malloc'd string -> Needs to be freed!
 */
string str_concat(string str1, string str2);

/*
 * Get substring of given string. Similar function to basic_string::substr in C++11.
 *
 * @param str String to use.
 * @param start Index to start substring at. Starts at 0.
 * @param num Number of characters to copy.
 * @return Returns new string containing requested substring.
 */
string str_substr(string str, int start, int num);

/*
 * Replaces the last occurrence of the given character by the given character.
 *
 * @param str String to operate on.
 * @param s Char to replace.
 * @param rep What to replace it with.
 * @return Returns a pointer to the replaced character, or a NULL, if unable to replace.
 */
char* str_replace_last(string str, char s, char rep);

/*
 * Deletes from the end of the string down to the given character.
 *
 * @param str String to operate on.
 * @param s Character to delete down to.
 * @return Returns pointer to the character, or NULL, if the character was not found.
 */
char* str_delete_downto(string str, char s);

/*
 * Print content of string
 *
 * @param str1 String to be printed
 */
void str_print(string str1);

#endif