#!/bin/sh

#Script expects ifj exe in same folder

#then for each test is requered sub folder with document with IFJ15 code in it and .ifj
#subfolder has to have same name as .ifj file

#Script makes .output file with stdout and stderr in it

#no check, if files or folders exist

if [ "$1" = "-gcc" ]; then #compile and run testing code with g++

	for D in $(ls -d */) #makes action over everz subfolder in current folder
	do
		RC="$D$(echo $D | sed 's/\//./')ifj" #path to .ifj file
		OP="$D$(echo $D | sed 's/\//./')output" #path to .output file
		CC="$D$(echo $D | sed 's/\//./')cc" #path to .cc file
		N="$D$(echo $D | sed 's/\///')" #path to g++ exe
		NOP="$D$(echo $D | sed 's/\//./')ccoutput" #path to g++ output
		IN="${D}in.stdin"

		echo
		echo Provadim g++ nad $RC

		if [ -f "$RC" ] && [ -f "$IN" ] ; then
			cat ifj15.cc $RC > $CC
			g++ -std=c++11 -o $N $CC
			./$N > $NOP 2>&1 < $IN
			wait
		else
			echo Soubor $RC nebo $IN neexistuje
		fi
	done

elif [ "$1" = "-n" ] ; then #run testing code with ./ifj

	for D in $(ls -d */) #makes action over everz subfolder in current folder
	do
		RC="$D$(echo $D | sed 's/\//./')ifj" #
		OP="$D$(echo $D | sed 's/\//./')output"
		IN="${D}in.stdin"

		echo
		echo Provadim interpretaci $RC

		if [ -f "$RC" ] && [ -f "$IN" ] ; then
			./ifj $RC > $OP 2>&1 < $IN
			wait
		else
			echo Soubor $RC nebo $IN neexistuje
		fi
	done

elif [ "$1" = "-c" ] ; then #cleans folders

	for D in $(ls -d */) #makes action over everz subfolder in current folder
	do
		OP="$D$(echo $D | sed 's/\//./')output" #path to .output file
		CC="$D$(echo $D | sed 's/\//./')cc" #path to .cc file
		N="$D$(echo $D | sed 's/\///')" #path to g++ exe
		NOP="$D$(echo $D | sed 's/\//./')ccoutput" #path to g++ output
		OP="$D$(echo $D | sed 's/\//./')output"
		DIFF="${D}diff.diff"

		echo
		echo Provadim cistku nad $D

		rm $OP $CC $N $NOP $OP $DIFF -f
		wait
	done

elif [ "$1" = "-diff" ] ; then

	for D in $(ls -d */) #makes action over everz subfolder in current folder
	do
		OP="$D$(echo $D | sed 's/\//./')output" #path to .output file
		NOP="$D$(echo $D | sed 's/\//./')ccoutput" #path to g++ output
		DIFF="${D}diff.diff"

		echo 
		echo Provadim diff pro $D

		if [ -f "$OP" ] && [ -f "$NOP" ] ; then
			diff $OP $NOP > $DIFF
			wait
			cat $DIFF
		else
			echo Neexistuji porovnavaci soubory, zavolejte me s parametrem -n and -gcc
		fi

	done


else

	echo chybi argument

fi

echo