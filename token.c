#include "token.h"

/*
 * Get name for given token type.
 *
 * @param type Type to process.
 * @return Returns printable string with types name.
 */
const char* token_type_name(token_type type)
{
    switch(type)
    {
        case type_identifier :
            return "Identifier";
        case type_identifier_fun :
            return "Function identifier";
        case type_string_lit :
            return "String literal";
        case type_int_lit :
            return "Integer literal";
        case type_double_lit :
            return "Double literal";
        case type_bracket_open :
            return "(";
        case type_bracket_close :
            return ")";
        case type_equal :
            return "=";
        case type_dequal :
            return "==";
        case type_greater :
            return ">";
        case type_lesser :
            return "<";
        case type_gequal :
            return ">=";
        case type_lequal :
            return "<=";
        case type_nequal :
            return "!=";
        case type_stream_left :
            return "<<";
        case type_stream_right :
            return ">>";
        case type_plus :
            return "+";
        case type_minus :
            return "-";
        case type_star :
            return "*";
        case type_slash :
            return "/";
       /* case type_excl_mark :
            return "!";*/
        case type_brace_open :
            return "{";
        case type_brace_close :
            return "}";
        case type_semicolon :
            return ";";
        case type_comma :
            return ",";
        case type_kw_auto :
            return "Type auto";
        case type_kw_int :
            return "Type int";
        case type_kw_string :
            return "Type string";
        case type_kw_double :
            return "Type double";
        case type_kw_cin :
            return "Cin";
        case type_kw_cout :
            return "Cout";
        case type_kw_else :
            return "Else";
        case type_kw_for :
            return "For";
        case type_kw_if :
            return "if";
        case type_kw_return :
            return "Return";
        case type_kw_while :
            return "While";
        case type_kw_do :
            return "Do";
        case type_eof :
            return "EOF";
        default :
            return "Unknown token type!";
    }
}
