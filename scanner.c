#include "scanner.h"
#include "stdio.h"
#include "string.h"
#include "token.h"


typedef enum
{
    s_default,
    s_identifier,
    s_equal,
    s_greater,
    s_lesser,
    s_excl_mark,
    s_num_lit,
    s_num_point,
    s_num_fract, // Fractional part of number, everything after '.' and before 'e'
    s_num_exp,
    s_num_exp_first,
    s_num_exp_val,
    s_string_lit,
    s_string_escaped,
    s_string_hex1,
    s_string_hex2,
    s_slash,
    s_comment,
    s_comment_end,
    
} state_t;


// File being read
FILE* source;
// Default buffer for parsing current token
string str;
/*  Helper buffer for parsing sub-expressions(hexadecimal string entry)
    eg: in "abc\FF\32" this buffer will be used to parse FF and 32 */
string help_buf;

int line_num;
int char_num;
bool scanner_init(const char* filename)
{
    source = fopen(filename,"r");
    str = tstr_init();
    help_buf = tstr_init();
    line_num=1;
    char_num=0;
    return source != NULL;
}

/*
 * Free resources used by scanner
 */
void scanner_free()
{
    tstr_free(str);
    tstr_free(help_buf);
    fclose(source);
}

/* Used for storing last haracter in case it does not belong to currently processed token
*/
static int backup = 0;
/* Returns next character from file or EOF */
static int get_next()
{
    if(backup)
    {
        int res = backup;
        backup = 0;
        return res;
    }
    char_num++;
    return fgetc(source);
}
static void backup_char(char c)
{
    backup = c;
}
token_t parse_double()
{
    token_t token;
    token.type=type_double_lit;

    tstr_push(str,0);
    double res = strtod(str->data,0);
    if(errno == ERANGE || res < 0)
        fail(ERR_LEXICAL, "double out of range", line_num, char_num);
    tstr_pop(str);
    token.double_lit=res;
    return token;
}
token_type get_kw_type(char* kw)
{   
    if(str_eq(kw,"auto"))
        return type_kw_auto;
    else if(str_eq(kw,"return"))
        return type_kw_return;
    else if(str_eq(kw,"cin"))
        return type_kw_cin;
    else if(str_eq(kw,"cout"))
        return type_kw_cout;
    else if(str_eq(kw,"if"))
        return type_kw_if;
    else if(str_eq(kw,"else"))
        return type_kw_else;
    else if(str_eq(kw,"for"))
        return type_kw_for;
    else if(str_eq(kw,"int"))
        return type_kw_int;
    else if(str_eq(kw,"double"))
        return type_kw_double;
    else if(str_eq(kw,"string"))
        return type_kw_string;
    else if(str_eq(kw,"while"))
        return type_kw_while;
    else if(str_eq(kw,"do"))
        return type_kw_do;

    return type_identifier;
}

token_t get_token()
{
    // We use 2 states, current and next one to avoid modifying current state
    // That could lead to errors
    state_t state = s_default;
    state_t next_state = state;
    tstr_clear(str);
    tstr_clear(help_buf);
    int c = 0 ;
    token_t token = {0};
    while(true)
    {
        state=next_state;
        c = get_next();

        token.line_num=line_num;
        token.char_num=char_num;

        // Moved the EOF inside the if
        if(state==s_default)
        {
            if(c==EOF)
            {
                // End of file, return empty token
                token_t res;
                res.type=type_eof;
                return res;
            }
            else if(c == '\n')
            {
                char_num=0;
                line_num++;
            } 
            else if(c == ' ' || c == '\t' || c == 13)
            {}
            else if(is_character(c) || c=='_')
            {
                //identifier or keyword
                tstr_push(str,c);
                next_state=s_identifier;
            }
            else if (is_digit(c))
            {
                // int or double literal
                tstr_push(str,c);
                next_state=s_num_lit;
            }
            else if(c == '(') {token.type=type_bracket_open; return token; }
            else if(c == ')') {token.type=type_bracket_close; return token; }
            else if(c == '{') {token.type=type_brace_open; return token; }
            else if(c == '}') {token.type=type_brace_close; return token; }
            else if(c == ';') {token.type=type_semicolon; return token; }
            else if(c == ',') {token.type=type_comma; return token; }
            else if(c == '=') {next_state =s_equal;}
            else if(c == '>') {next_state =s_greater; }
            else if(c == '<') {next_state =s_lesser; }
            else if(c == '+') {token.type=type_plus; return token; }
            else if(c == '-') {token.type=type_minus; return token; }
            else if(c == '*') {token.type=type_star; return token; }
            else if(c == '!') {next_state =s_excl_mark; }   
            else if(c == '/') {next_state =s_slash; }
            else if(c == '\"'){next_state=s_string_lit; }
            else {
                fail(ERR_LEXICAL,"Unknown character",line_num,char_num);
            }
        }
        else if (state == s_identifier)
        {
            if (is_character(c) || is_digit(c) || (c=='_'))
            {
                // Character, digit or underscore are simply added to buffer
                tstr_push(str,c);
            }
            else
            {
                //Encountered char that does not belong to identifier
                backup_char(c);


                token_t newTok;

                newTok.line_num = token.line_num;
                newTok.char_num = token.char_num;
                tstr_push(str,0);
                newTok.type = get_kw_type(str->data);
                tstr_pop(str);
                if(newTok.type==type_identifier)
                    newTok.identifier = tstr_duplicate(str);
                tstr_clear(str);
                return newTok;
            }
        }
        else if(state==s_equal)
        {
            if (c == '=')
            { /* Double equal == */
                next_state=s_default;
                token.type=type_dequal;
                return token;
            }
            else
            { /* Single equal = */
                next_state=s_default;
                backup_char(c);
                token.type=type_equal;
                return token;
            }
        }
        else if(state==s_greater)
        {
            switch(c)
            {
                case '>':
                    token.type=type_stream_right;
                    return token;
                case '=':
                    token.type=type_gequal;
                    return token;
                default:
                    backup_char(c);
                    token.type=type_greater;
                    return token;
                break;
            }
        }
        else if(state==s_lesser)
        {
            switch(c)
            {
                case '<':
                    token.type=type_stream_left;
                    return token;
                case '=':
                    token.type=type_lequal;
                    return token;
                default:
                    backup_char(c);
                    token.type=type_lesser;
                    return token;
                break;
            }
        }
        else if(state==s_excl_mark)
        {
            if(c=='=')
            {
                token.type=type_nequal;
                return token;
            }
            fail(ERR_LEXICAL,"Expected =",line_num,char_num);
           /* backup_char(c);
            token.type= type_excl_mark;
            return token;*/
        }
        else if(state==s_slash)
        {
            if(c=='*')//Star, multiline comment
            {
                next_state=s_comment;
            }
            else if(c == '/') //another slash, single line comment
            {
                while(c != '\n')//Read line until end
                {
                    c = get_next();
                }
                line_num++;
                char_num = 0;
                next_state=s_default;
            }
            else
            {
                /* Just single slash, not representing comment */
                backup_char(c);
                token.type = type_slash;
                return token;
            }
        }
        else if(state==s_comment)
        {
            if(c == '*') // Star, maybe this is the end of the comment
                next_state=s_comment_end;
            else if(c == '\n')
            {
                line_num++;
                char_num = 0;
            }
            else if(c == EOF)
                fail(ERR_LEXICAL, "found EOF before comment end", line_num, char_num);
        }
        else if(state==s_comment_end)
        {
            if(c=='/')//Comment has ended
            {
                next_state=s_default;
            }
            else if(c == EOF)
                fail(ERR_LEXICAL, "found EOF before comment end", line_num, char_num);
            else /* comment contuing */
            {
                if(c == '\n')
                {
                    line_num++;
                    char_num = 0;
                }
                next_state=s_comment;
            }
        }
        else if(state==s_string_lit)
        {
            if(c== '\n')/* Actual newline appeared in source, we should ignore it */
            {

            }
            else if(c =='\\')//Escaped sequence
                next_state=s_string_escaped;
            else if(c == '\"')//End of string literal
            {

                token_t token;
                token.type=type_string_lit;
                token.string_lit=tstr_duplicate(str);
                tstr_clear(str);
                return token;
            }
            else if(c == EOF)
                fail(ERR_LEXICAL, "Found end of file, while parsing string literal", token.line_num, token.char_num);
            else
            { /* Any other character is part of string literal */
                tstr_push(str,c);
            }
        }
        else if(state==s_string_escaped) 
        {
            /* Escaped string, we already parsed '\' , now we are parsing second part */
            if(c=='n')
            { /* Newline */
                tstr_push(str,'\n');
                next_state=s_string_lit;
            }
            else if(c == 't')
            { /* Tab */
                tstr_push(str,'\t');
                next_state=s_string_lit;
            }
            else if(c == '\\' || c == '\"')
            { /* Escaped " and \ , just put them in buffer */
                tstr_push(str,c);
                next_state=s_string_lit;
            }
            else if(c=='x')
            { 
                next_state=s_string_hex1;
            }
            else //Lexical error
            {

                fail(ERR_LEXICAL,"Unknown character",line_num,char_num);
                //Error happened, we try to continue parsing
                next_state=s_string_lit;
            }
        }
        else if(state==s_string_hex1)
        {
            if(is_hex(c))
            {
                tstr_push(help_buf, c );
                next_state=s_string_hex2;
            }
            else
            {
                //Error happened, we try to continue parsing
                fail(ERR_LEXICAL,"Unknown character",line_num,char_num);
                next_state=s_string_lit;
            }
        }
        else if(state==s_string_hex2)
        {
            if(is_hex(c))
            {
                tstr_push(help_buf, c );
                tstr_push(help_buf, 0 );

                int val = strtol(help_buf->data,NULL,16);
                if(val == 0)
                    fail(ERR_LEXICAL, "Entering \\x00 is forbidden", line_num, char_num);
                tstr_push(str,val);
                tstr_clear(help_buf);
                next_state=s_string_lit;
            }
            else //Lexical error
            {
                fail(ERR_LEXICAL,"Unknown character",line_num,char_num);
                next_state=s_string_lit;
            }
        }
        else if(state==s_num_lit)
        {
            if(is_digit(c))
            {
                tstr_push(str,c);
            }
            else if(c == '.')
            {
                tstr_push(str,c);
                next_state=s_num_point;
            }
            else if(c == 'e' || c == 'E')
            {
                tstr_push(str,c);
                next_state= s_num_exp;
            }
            else /* Unknown character, ending this token */
            {
                backup_char(c);
                token.type=type_int_lit;
                tstr_push(str,0);
                token.int_lit=strtol(str->data,NULL,0);
                if(errno == ERANGE || token.int_lit < 0)
                    fail(ERR_LEXICAL, "integer out of range", line_num, char_num);
                return token;
            }
        }
        else if(state==s_num_point)
        {
            if(is_digit(c))
            {
                tstr_push(str,c);
                next_state=s_num_fract;
            }
            else if(c == 'e' || c=='E') /* Error, no numbers in fractional part, corresponds to 5.e3 */
            {
                /* We try to resolve it */
                fail(ERR_LEXICAL,"Missing digits",line_num,char_num);
                tstr_push(str,c);
                next_state=s_num_exp;
            }
            else /* Totally unknown character corresponds to  something like: 5.; */
            {
                fail(ERR_LEXICAL,"Missing digits",line_num,char_num);
                backup_char(c);
                return parse_double();
            }
        }
        else if(state==s_num_fract)
        {
            if(is_digit(c))
            {
                tstr_push(str,c);
            }
            else if(c == 'e' || c=='E')
            {
                tstr_push(str,c);
                next_state=s_num_exp;
            }   
            else /*  corresponds to 5.8  */
            {
                backup_char(c);
                return parse_double();
            }
        }
        else if(state==s_num_exp)
        {
            if(c=='+' || c == '-') // 5e+
            {
                tstr_push(str,c);
                next_state=s_num_exp_first;
            }
            else if(is_digit(c)) // 5e3
            {
                tstr_push(str,c);
                next_state=s_num_exp_val;
            }
            else // 5e;
            {
                fail(ERR_LEXICAL,"Unknown character",line_num,char_num);
                backup_char(c);
                return parse_double();
            }
        }
        else if(state==s_num_exp_first)
        {
            if(is_digit(c)) // 5e33
            {
                tstr_push(str,c);
                next_state=s_num_exp_val;
            }
            else 
            {
                fail(ERR_LEXICAL,"Missing digits",line_num,char_num);
                backup_char(c);
                return parse_double();
            }
        }
        else if(state==s_num_exp_val)
        {
            if(is_digit(c)) // 5e3...
            {
                tstr_push(str,c);
                next_state=s_num_exp_val;
            }
            else
            {
                backup_char(c);
                return parse_double();
            }

        }

    }
    return token;
}
