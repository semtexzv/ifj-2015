#include <stdio.h>
#include "main.h"

#include "parser.h"
#include "scanner.h"
#include "interpret.h"
#include "global.h"

void fail(int type, const char* desc, int line_num, int row_num)
{   
    char* errType = "";
    switch(type)
    {
        case ERR_LEXICAL:
            errType= "Lexical Error";
            break;
        case ERR_SYNTAX:
            errType= "Syntax Error";
            break;
        case ERR_SEMANTIC:
            errType= "Semantic Error";
            break;
        case ERR_TYPE_MISMATCH:
            errType= "Type Mismatch";
            break;
        case ERR_TYPE_UNKNOWN:
            errType= "Unknown Type";
            break;
        case ERR_SEMANTIC_OTHER:
            errType= "Semantic Error";    
            break;
        case ERR_RUNTIME_INPUT:
            errType= "Input Error";
            break;
        case ERR_UNINITALIZED_VAR:
            errType= "Uninitialized variable";
            break;
        case ERR_DIV_ZERO:
            errType= "Zero division";
            break;
        case ERR_RUNTIME_OTHER:
            errType= "Runtime Error";
            break;
        default:
            errType= "Internal Error";
            break;
    }
    if(line_num != 0)
        fprintf(stderr, "%s %d:%d-%s\n",errType, line_num,row_num,desc);
    else
        fprintf(stderr, "%s %s\n", errType, desc);
    exit(type);
}

int main(int argc, char const *argv[])
{
    global_init();
    if(argc < 2)
    {
        //We didnt get any file to interpret
        fprintf(stderr, "No source files provided, stopping\n");
        return ERR_INTERNAL;
    }
    if(!scanner_init(argv[1]))
    {
        //Could not open file
        fprintf(stderr, "Error opening source file, stopping\n");
        return ERR_INTERNAL;
    }
    
    parser_init();
    parse();

    interpret_init();
    interpret();

    scanner_free();
    parser_free();
    interpret_free();
    global_free();

    return 0;
}
