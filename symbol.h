#ifndef SYMBOL_H
#define SYMBOL_H

#include <stdint.h>
#include "string.h"
#include "instr.h"
#include "token.h"

struct TSymbolData;
typedef struct TSymbolData* symbol_ptr;

#define VEC_DATA_TYPE symbol_ptr
#define VEC_DEFAULT_VAL 0
#include "vec_base.ht"
#undef VEC_DEFAULT_VAL
#undef VEC_DATA_TYPE

struct tFunctionData;
struct TVarData;
struct TConstData;
struct TSymbolTable;

typedef enum 
{
    sym_function,
    sym_var,
    sym_const,
} symbol_type;

typedef struct TFunctionData
{
    /* Should contain pointers to metadata about function parameters */
    vec_symbol_ptr *params;
    /* Size of memory to allocate when calling this function */
    int stack_size;
    /* Return type of this function */
    token_type ret_type;
    string name;
    /* Actual code of this function */
    union
    {
        vec_instr * code;
        void(*intFun)(void);
    };
    bool defined;
    bool internal;

} tFunction;

typedef struct TVarData
{
    /* Data type of this variable*/
    token_type type;
    /* Used for temporary identification of parameters */
    string name;
    /* Position on the stack,  1st param should be at position 0, 2nd at position 1
       and so on  */
    int mem_offset;
} tVariable;

typedef struct TConstData
{
    /* Data type , int , double or string */
    token_type type;
    /* Actual data of this constant */
    union
    {
        int int_val;
        double double_val;
        string string_val;
    };
} tConstant;

typedef struct TSymbolData
{
    /* type of this symbol */
    symbol_type type;
    union
    {
        tFunction function; 
        tVariable variable;
        tConstant constant;
    };
} symbol;



#define VEC_DATA_TYPE symbol
#define VEC_DEFAULT_VAL 0
#include "vec_base.ht"
#undef VEC_DEFAULT_VAL
#undef VEC_DATA_TYPE

// Used as container in hash table
typedef struct TSymbolElement
{
    string key;
    symbol_ptr data;
    struct TSymbolElement* next;
} symbol_el;

typedef struct TSymbolElement* symbol_el_ptr;

/*
 * Free given symbol.
 *
 * @param sym Symbol to free
 */
void sym_free(symbol_ptr sym);

/*
 * Print debug info about given symbol.
 *
 * @param sym Symbol to print debug info about
 */
void sym_debug(symbol_ptr sym);

#endif
