# LL Grammar

Written in format compatible with :
http://mdaines.github.io/grammophone/#/


program -> fun_dd .
fun_dd -> type id ( param ) f_spec .
param -> type id param_l .
param -> .
param_l -> , type name param_l .
param_l -> .
f_spec -> cmp_st .
f_spec -> ; .

cmp_st -> { st_list } .
st_list -> st st_list .
st_list -> .

st -> assign ; .
st -> cmp_st .
st -> if_st .
st -> for_st .
st -> var_dd ; .
st -> st_in ; .
st -> st_out ; .
st -> st_while .
st -> st_dowhile ;.

assign -> id = expr .

var_dd -> type_a id var_spec .
var_spec -> = expr .
var_spec -> .

st_in -> cin >> id st_in_l .
st_in_l -> >> id st_in_l .
st_in_l -> .

st_out -> cout << expr st_out_l .
st_out_l -> << expr st_out_l .
st_out_l -> .

if_st -> if ( expr ) st else_st .
if_else -> else st | .

for_st -> for ( var_dd ; expr ; assign ) st .

st_while -> while ( expr ) st .
st_dowhile -> do st while ( expr ) .