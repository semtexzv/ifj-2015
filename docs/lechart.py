from pygraphviz import *

G=AGraph(directed=True,dpi=2000,size="8,5",overlap="false",splines="true",rotate=90,rankdir="LR")

G.add_node("default")
G.add_node("(",shape='doublecircle')
G.add_node(")",shape='doublecircle')
G.add_node("{",shape='doublecircle')
G.add_node("}",shape='doublecircle')
G.add_node(";",shape='doublecircle')
G.add_node(",",shape='doublecircle')
G.add_node("=",shape='doublecircle')
G.add_node("==",shape='doublecircle')
G.add_node(">",shape='doublecircle')
G.add_node("<",shape='doublecircle')
G.add_node("+",shape='doublecircle')
G.add_node("-",shape='doublecircle')
G.add_node("*",shape='doublecircle')
G.add_node(">>",shape='doublecircle')
G.add_node(">=",shape='doublecircle')
G.add_node("<<",shape='doublecircle')
G.add_node("<=",shape='doublecircle')
G.add_node("!=",shape='doublecircle')
G.add_node("!")
G.add_node("/",shape='doublecircle')
G.add_node("comment")
G.add_node("lcomment")
G.add_node("identifier",shape='doublecircle')
G.add_node("string_lit")
G.add_node("string_lit_end",shape='doublecircle')
G.add_node("string_escaped")
G.add_node("string_hex")
G.add_node("num_lit",shape='doublecircle')
G.add_node("num_point")
G.add_node("num_exp")
G.add_node("num_frac",shape='doublecircle')
G.add_node("num_exp_first")
G.add_node("num_exp_val",shape='doublecircle')
G.add_node("lex_error",shape='doublecircle')


G.add_edge('default','default',label=r"\\n | ' ' | \\t ")
G.add_edge('default','identifier',label=r"char | _")
G.add_edge('default','num_lit',label=r"digit")
G.add_edge('default','(',label="(")
G.add_edge('default',')',label=")")
G.add_edge('default','{',label=r"{")
G.add_edge('default','}',label=r"}")
G.add_edge('default',';',label=r"';")
G.add_edge('default',',',label=r",")
G.add_edge('default','=',label=r"=")
G.add_edge('default','>',label=r">")
G.add_edge('default','<',label=r"<")
G.add_edge('default','+',label=r"+")
G.add_edge('default','-',label=r"-")
G.add_edge('default','*',label=r"*")
G.add_edge('default','!',label=r"!")
G.add_edge('default','/',label=r"/")
G.add_edge('default','string_lit',label=r"\"")
G.add_edge('default','lex_error',label=r"other")

G.add_edge('identifier','identifier',label=r"char | digit | _")

G.add_edge('=','==',label=r"=")

G.add_edge('>','>>',label=r">")
G.add_edge('>','>=',label=r"=")

G.add_edge('<','<<',label=r"<")
G.add_edge('<','<=',label=r"=")

G.add_edge('!','!=',label=r"'='")
G.add_edge('!','lex_error',label=r"other")

G.add_edge('/','comment',label=r"*")
G.add_edge('/','lcomment',label=r"/")

G.add_edge('lcomment','lcomment',label=r"other")
G.add_edge('lcomment','default',label=r"\\n")

G.add_edge('comment','comment_end',label=r"\*")
G.add_edge('comment','comment',label=r"other")
G.add_edge('comment','lex_error',label=r"EOF")

G.add_edge('comment_end','default',label=r"/")
G.add_edge('comment_end','comment',label=r"other")
G.add_edge('comment_end','lex_error',label=r"EOF")

G.add_edge('string_lit','string_escaped',label=r" \\ ")
G.add_edge('string_lit','string_lit_end',label=r"\"")
G.add_edge('string_lit','string_lit',label=r"other")
G.add_edge('string_lit','lex_error',label=r"EOF")

G.add_edge('string_escaped','string_lit',label=r"n | t | \\ | \" ")
G.add_edge('string_escaped','string_hex',label=r"x")
G.add_edge('string_escaped','lex_error',label=r"other")

G.add_edge('string_hex','string_hex2',label=r"hex")
G.add_edge('string_hex','lex_error',label=r"other")

G.add_edge('string_hex2','string_lit',label=r"hex")
G.add_edge('string_hex2','lex_error',label=r"other")

G.add_edge('num_lit','num_lit',label=r"digit")
G.add_edge('num_lit','num_point',label=r"\.")
G.add_edge('num_lit','num_exp',label=r"e | E")

G.add_edge('num_point','num_frac',label=r"digit")
G.add_edge('num_point','lex_error',label=r"other")

G.add_edge('num_frac','num_frac',label=r"digit")
G.add_edge('num_frac','num_exp',label=r"e | E")

G.add_edge('num_exp','num_exp_first',label=r"\+ | \-")
G.add_edge('num_exp','num_exp_val',label=r"digit")
G.add_edge('num_exp','lex_error',label=r"other")

G.add_edge('num_exp_first','num_exp_val',label=r"digit")
G.add_edge('num_exp_first','lex_error',label=r"other")

G.add_edge('num_exp_val','num_exp_val',label=r"digit")


G.layout(prog='dot')
G.draw('lexChart.jpg')
G.write('lexChart.dot')