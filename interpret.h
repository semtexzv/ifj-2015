#ifndef INTERPRET_H
#define INTERPRET_H

#include <ctype.h>
#include <errno.h>
#include "main.h"
#include "global.h"


#define OPERATE(op1, op2, operator, op3, type)\
do{\
    if(type == type_kw_int)\
        op1->int_val = op2->int_val operator op3->int_val;\
    else\
        op1->double_val = op2->double_val operator op3->double_val;\
}while(0)

/* Internal function callers */

/* int length(string s), string substr(string s, int i, int n), string concat(string s1, string s2,
 * int find(string s, string search), string sort(string s) */
void caller_length();
void caller_substr();
void caller_concat();
void caller_find();
void caller_sort();

/*
 * Check, if function, given by symbol pointer is defined.
 * Has no effect for non-function symbols. If there were non-defined functions exits the program.
 *
 * @param sym Symbol to check.
 */
static inline void sym_chk_fun(symbol_ptr sym)
{
    if(sym->type == sym_function)
    {
        if(!sym->function.defined)
        {
            LOG_DBG("Function \"%s\" undefined!\n", str_to_c_str(sym->function.name));
            fail(ERR_SEMANTIC, "Interpretation cannot begin - declared function undefined", 0, 0);
        }
        /* Check, if the last instruction is return */
        /* Moved to INSTR_FUN_END
        if(sym->function.internal != true && vec_instr_top(sym->function.code)->type != INSTR_RET)
            fail(ERR_UNINITALIZED_VAR, "Function is missing return", 0, 0);
            */

        /* Check all parameters, if they don't have same name as one of the functions */
        if(!sym->function.internal)
        {
            symbol_ptr param = NULL;
            symbol_ptr found = NULL;
            for(int iii = 0; iii < vec_symbol_ptr_size(sym->function.params); ++iii)
            {
                param = *vec_symbol_ptr_at(sym->function.params, iii);
                found = lookup_fun(param->variable.name);
                if(found)
                    fail(ERR_SEMANTIC, "Parameter name same as one of the function names", 0, 0);
            }
        }

    }
    else if(sym->type == sym_var)
    {
        /* Check, if function with the same name exists */
        symbol_ptr found = lookup_fun(sym->variable.name);
        if(found)
            fail(ERR_SEMANTIC, "Variable name is the same as function name", 0, 0);
    }
}

/*
 * Interpret initialization.
 */
void interpret_init();

/*
 * Main interpreting routine.
 */
void interpret();

/*
 * Free interpret structures.
 */
void interpret_free();

#endif
