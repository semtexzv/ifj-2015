#include "instr.h"

const char * INSTR_NAMES[] = {
        "INSTR_NOP",

        "INSTR_MOV",
        "INSTR_OUT",
        "INSTR_IN",

        "INSTR_CVTID",
        "INSTR_CVTDI",

        "INSTR_RET",
        "INSTR_FR_PREP",
        "INSTR_FR_PARAM",
        "INSTR_FR_CALL",

        "INSTR_PREP_VAR",

        "INSTR_JMP",
        "INSTR_JMPC",

        "INSTR_CMP_EQ", 
        "INSTR_CMP_NEQ",
        "INSTR_CMP_GT" ,
        "INSTR_CMP_GEQ" ,
        "INSTR_CMP_LT",
        "INSTR_CMP_LEQ",

        "INSTR_AND",
        "INSTR_OR",
        "INSTR_NOT",
        "INSTR_XOR",

        "INSTR_ADD",
        "INSTR_SUB",
        "INSTR_MUL",
        "INSTR_DIV",

        "INSTR_DBG",

        "INSTR_FUN_END"
};

#define VEC_DATA_TYPE instr
#define VEC_DEFAULT_VAL {0}
#include "vec_base.ct"
#undef VEC_DEFAULT_VAL
#undef VEC_DATA_TYPE

/*
 * Print debug information about given instruction.
 *
 * @param instr Instruction to print info about
 */
void instr_debug(instr_ptr instr)
{
    LOG_DBG("Instr code : %d", instr->type);
    LOG_DBG("Instr : %s(%p %p %p)\n", instr_get_name(instr->type), (void*)instr->first, (void*)instr->second, (void*)instr->third);
}
