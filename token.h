#ifndef TOKEN_H
#define TOKEN_H
#include "string.h"

typedef enum 
{
    type_identifier=1,
    type_identifier_fun,
    type_string_lit, /* string literal  */
    type_int_lit, /* integer literal */
    type_double_lit, /* floating point literal */
    type_bracket_open, /* ( */
    type_bracket_close,/* ) */
    type_equal, /* = */
    type_dequal, /* == */
    type_greater, /* > */
    type_lesser, /* < */
    type_gequal, /* >= */
    type_lequal, /* <= */
    type_nequal, /* != */
    type_stream_left, /* << */
    type_stream_right, /* >> */
    type_plus, /* + */ 
    type_minus, /* - */ 
    type_star, /* * */ 
    type_slash, /* / */
   // type_excl_mark, /* ! */
    type_brace_open, /* { */
    type_brace_close, /* } */
    type_semicolon, /* ; */
    type_comma, /* , */

    /*Keywords*/
    type_kw_auto,
    type_kw_int,
    type_kw_string,
    type_kw_double,

    type_kw_cin,
    type_kw_cout,
    type_kw_else,
    type_kw_for,
    type_kw_if,
    type_kw_return,

    type_kw_while,
    type_kw_do,
    
    type_eof=99, /*End of file */

} token_type;


typedef struct 
{
    token_type type; // Token type
    union
    {
        string identifier;
        string string_lit;
        int int_lit;
        double double_lit;
    };
    int line_num;
    int char_num;
} token_t;

/*
 * Get name for given token type.
 *
 * @param type Type to process.
 * @return Returns printable string with types name.
 */
const char* token_type_name(token_type type);

#endif