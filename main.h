#ifndef COMMON_H
#define COMMON_H

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>

#define ERR_LEXICAL 1
#define ERR_SYNTAX 2
#define ERR_SEMANTIC 3
#define ERR_TYPE_MISMATCH 4
#define ERR_TYPE_UNKNOWN 5
#define ERR_SEMANTIC_OTHER 6
#define ERR_RUNTIME_INPUT 7
#define ERR_UNINITALIZED_VAR 8
#define ERR_DIV_ZERO 9
#define ERR_RUNTIME_OTHER 10
#define ERR_INTERNAL 99

#define LOG_MSG(type, format, ...) \
    fprintf(stderr, "[" type "] %s(%d) : " format "\n", \
            __FILE__, __LINE__, ##__VA_ARGS__)

#ifdef DEBUG
#define LOG_DBG(format, ...) LOG_MSG("DEBUG", format, ##__VA_ARGS__)
#else
#define LOG_DBG(...)
#endif

#define LOG_WRN(format, ...) LOG_MSG("WARNING", format, ##__VA_ARGS__)
#define LOG_ERR(format, ...) LOG_MSG("ERROR ", format, ##__VA_ARGS__)

#define CHK_ALLOC(var) \
    if(!(var)) { \
        LOG_ERR("%s", "Allocation failed!"); \
        exit(ERR_INTERNAL); \
    }

/* Helper macros */
#define is_digit(x) (x >= '0' && x <='9')
#define is_lower(x) (x >= 'a' && x <='z')
#define is_upper(x) (x >= 'A' && x <= 'Z')
#define is_character(x) (is_lower(x) || is_upper(x))
#define is_hex(x) ( is_digit(x) || (x >= 'a' && x<='f') || (x >= 'A' && x <='F'))
#define str_eq(x,y) (strcmp(x,y)==0)

void fail(int type, const char* desc, int line_num, int row_num);

#endif
